# portfolio

The personal portfolio of Zachariah Dzielinski

## Development

### Requirements

* [hugo](https://gohugo.io/getting-started/installing/)

### Rendering Locally

```bash
hugo serve
```

## License

* [Project license](./LICENSE.md)

### Copyright Notices

* [Hello Friend NG](./themes/hugo-theme-hello-friend-ng/LICENSE.md)