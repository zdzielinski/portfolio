+++
title = "About Me"
date = "2021-03-26"
aliases = ["about"]
[ author ]
  name = "Zachariah Dzielinski"
+++

Hey there!

You may have come to this page to gain a better overview of my technical abilities, pieces of performance, or to obtain a deeper understanding of how I'd make an awesome addition to your team / project.

I talk shop a lot during the day, and in all honesty, I do a better job of it in person.

Rather than re-iterate what you'll inevitably find out about during our future conversations (because I can tell we're getting along already), I'm going to outline some of the things that I credit to shaping my personality and character.

## Rock Climbing

---

When I packed my Honda Civic full to the brim and moved to Colorado, one of the main reasons why was because of my love of rock climbing. I've been finding other things to do lately, but it always holds a special place in my heart.

Throughout my tenure in this adventurous lifestyle, I've found myself spending almost every day either in the climbing gym or somewhere outside hanging on a wall of beautiful gritty stone.

I've had a blast improving not only as a climber but as a healthy and happy individual as well - rock climbing has certainly provided me with some of the most memorable moments of my life.

Fun fact - my wife and I got engaged at Earth Treks Columbia in Maryland :)

![](/img/zach_climbing_1.jpg)

## Snowboarding

---

You can blame my wife for this expensive hobby, though I certainly love it.

I'm definitely not a pro snowboarder, but especially since moving to Colorado, I've had the unique opportunity to improve at this sport as well. It's something I plan to push forward, although I'll usually opt for a day at the crag over a day in the mountains.

My wife and I plan on living out of Berthoud Pass and Salt Lake a few months out of the year to shred some killer pow. Thanks to the insanely drawn out process of our diesel heated Tacoma camper.

![](/img/zach_snowboarding_1.jpg)

## Hiking / Mountaineering

I've recently started checking off as many 14ers as I can, hopefully within the next 2-5 years; I can finish them all up! And then subsequently move on to knocking out the 637 13ers that Colorado calls her own.

Along with that, my wife and I backpack, thru hike, camp, etc. I grew up hiking in New Mexico and Colorado as a kid, when I was a boy scout (with better knees than today).

My love for the outdoors stemmed from activities like this during my life. I'm a huge proponent of leave no trace, and inclusivity in nature. Everyone deserves a chance to see the beauty I've been blessed with here!

![](/img/longs.jpg)

## Overlanding / Mechanic Work

We're currently in the process of building out my Tacoma into an overlander, it's fully custom and completely DIY. That, and a heck of a lot of work (I'm exhausted...). But, it's been fantastic.

Through this, and other cars I have - I've been falling in love with working with my hands. Doing all the suspension work myself, oil changes, engine and transmission work, electrics, you name it. If I could support myself working in an auto shop, I'd do it in a heartbeat.

![](/img/taco.jpg)

## HomeLab Hacking

---

I mean this goes without saying, I'm an absolute tech nerd.

My home lab has been a bit light to date, mainly due to the cargo limitations that came with moving via my Honda Civic. But I'm currently happy with my RPI Kubernetes cluster running a lot of my useful home automation software (Home Assistant, Paperless, etc).

But oh boy let me tell you - I've done my time with loud and power-hungry R710's and the like for years. I used to be an admin on a very exclusive HomeLab run out of Catonsville, MD via some of my college colleagues. Everything ran Arch Linux, and yes - it was fantastic to deal with.

![](/img/server.jpg)