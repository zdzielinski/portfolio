+++
title = "My Career"
date = "2021-09-14"
aliases = ["career"]
[ author ]
  name = "Zachariah Dzielinski"
+++

This is a trimmed down version of my resume, I'll happily send you the full one if you need it.

### **Principal Platform Engineer**

**[American Family Insurance](https://www.amfam.com/)**: Jan 2023 - Present

*Effort Group - [Data Science & Analytics Lab](https://amfamlabs.com/)*

---

### **Principal DevOps Engineer**

**[Grin](https://grin.co/)**: Sep 2021 - Jan 2023

---

### **Senior Consultant**

**[Oteemo](https://oteemo.com/)**: Apr 2021 - Sep 2021

---

### **Senior Cloud Engineer**

**[Booz Allen Hamilton](https://www.boozallen.com/)**: Nov 2020 - Apr 2021

*Prime Contract - [Unified Platform / Platform One](https://software.af.mil/team/platformone/)*

---

### **DevOps Engineer**

**[ClearEdge IT Solutions](http://clearedgeit.com/)**: May 2017 - Nov 2020

*Sub Contract - [Enlighten / DISA BDP](https://www.eitccorp.com/)*

---

### **System Administrator / Website Developer**

**[University of Maryland, Baltimore County](https://www.umbc.edu/)**: Jan 2016 - May 2017

*Effort Group - [UMBC CSEE](http://csee.umbc.edu/)*

---

### **DevOps Engineer / Website Developer**

**Boomspur, LLC**: Mar 2014 - Sep 2014

---

## Education

---

### **BSC - Information Systems**

*[University of Maryland, Baltimore County](https://www.umbc.edu/)*

---

### **ASC - Computer Science**

*[Hagerstown Community College](https://www.hagerstowncc.edu/)*
