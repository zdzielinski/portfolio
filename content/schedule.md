+++
title = "My Schedule"
date = "2021-08-22"
aliases = ["schedule"]
[ author ]
  name = "Zachariah Dzielinski"
+++

If you're feeling extroverted (I know I'm not), feel free to throw 30 minutes on my calendar.

Psst: if the widget below is broken or not rendering properly, [use my calendly link instead](https://calendly.com/zdzielinski/30min).

{{% calendly url="https://calendly.com/zdzielinski/30min?hide_event_type_details=1&hide_gdpr_banner=1" %}}
