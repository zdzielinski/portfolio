+++
title = "Contact Me"
date = "2021-03-26"
aliases = ["contact"]
[ author ]
  name = "Zachariah Dzielinski"
+++

If we end up getting along, I'd be happy to send you my cell phone number.

But for now, you can reach me via the following methods:

{{< fa fa-brands fa-linkedin >}} [LinkedIn](https://linkedin.com/in/zdzielinski) (Primary preference)

{{< fa fa-solid fa-envelope >}} <a class="encodedEmail" encodedEmail="{{% base64encode text=zach@dzielinski.dev %}}" href=".">An Email</a> (Secondary preference)

{{< fa fa-solid fa-house >}} [Snail Mail](https://goo.gl/maps/Kv85oVzwdnWBE6Bs9) (Why is this even here?)

{{< fa fa-solid fa-dove >}} [Carrier Pigeon](https://flypigeon.co/) (I'd be impressed)